-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2017 at 01:48 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donation_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `created_by`, `created_at`) VALUES
(2, 'personal', 'syeedmishu', '2017-05-17 09:42:54'),
(3, 'public', 'syeed', '2017-05-23 08:26:14');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `document_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `donation_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`document_id`, `name`, `type`, `donation_id`, `created_at`) VALUES
(1, 'Uploaded_files/dis.jpg', 'image/jpeg', 1, '2017-05-05 02:29:25'),
(2, 'Uploaded_files/Caribbean_Petroleum_Corporation_Disaster.jpg', 'image/jpeg', 1, '2017-05-05 02:29:26'),
(3, 'Uploaded_files/Caribbean_Petroleum_Corporation_Disaster1.jpg', 'image/jpeg', 2, '2017-05-05 03:01:23'),
(4, 'Uploaded_files/shutterstock_154310390.jpg', 'image/jpeg', 2, '2017-05-05 03:01:23'),
(5, 'Uploaded_files/dis4.jpg', 'image/jpeg', 3, '2017-05-15 02:38:32'),
(6, 'Uploaded_files/43400176-megaphone-man-character-promotion-speaking-news-stylized-making-sale-advertisement-announcement-gold2.jpg', 'image/jpeg', 4, '2017-05-15 02:51:33'),
(9, 'Uploaded_files/AbuSyeedAhmed_1yr.pdf', 'application/pdf', 6, '2017-05-17 08:57:29'),
(17, 'Uploaded_files/Abu_Syeed_new1.pdf', 'application/pdf', 10, '2017-05-17 09:39:43'),
(18, 'Uploaded_files/P_P_SIZE.jpg', 'image/jpeg', 10, '2017-05-17 09:39:43'),
(19, 'Uploaded_files/Abu_Syeed_new_Edited.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 11, '2017-05-17 09:46:25'),
(20, 'Uploaded_files/Check_Your_English_Vocabulary_for_IELTS.pdf', 'application/pdf', 12, '2017-05-17 14:00:26'),
(21, 'Uploaded_files/Check_Your_English_Vocabulary_for_IELTS1.pdf', 'application/pdf', 13, '2017-05-17 14:29:11'),
(22, 'Uploaded_files/Check_Your_English_Vocabulary_for_IELTS2.pdf', 'application/pdf', 14, '2017-05-17 14:30:44'),
(24, 'Uploaded_files/1.jpg', 'image/jpeg', 15, '2017-05-17 14:48:09'),
(25, 'Uploaded_files/3.jpg', 'image/jpeg', 15, '2017-05-17 14:48:09'),
(28, 'Uploaded_files/dis6.jpg', 'image/jpeg', 17, '2017-05-18 07:29:45'),
(29, 'Uploaded_files/Caribbean_Petroleum_Corporation_Disaster5.jpg', 'image/jpeg', 17, '2017-05-18 07:29:45'),
(38, 'Uploaded_files/shutterstock_1543103901.jpg', 'image/jpeg', 19, '2017-05-19 09:05:34'),
(40, 'Uploaded_files/dis9.jpg', 'image/jpeg', 19, '2017-05-19 09:06:22');

-- --------------------------------------------------------

--
-- Table structure for table `donates`
--

CREATE TABLE `donates` (
  `donate_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `donation_id` int(11) NOT NULL,
  `donating_amount` int(11) NOT NULL,
  `donation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donates`
--

INSERT INTO `donates` (`donate_id`, `user_id`, `donation_id`, `donating_amount`, `donation_time`) VALUES
(1, 74, 19, 0, '2017-05-26 03:26:21'),
(2, 74, 19, 400, '2017-05-26 03:26:36'),
(3, 74, 19, 500, '2017-05-26 03:28:08'),
(4, 74, 19, 500, '2017-05-26 03:28:11');

-- --------------------------------------------------------

--
-- Table structure for table `donations`
--

CREATE TABLE `donations` (
  `donation_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `donation_amount` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `address` text,
  `status_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donations`
--

INSERT INTO `donations` (`donation_id`, `title`, `description`, `donation_amount`, `category_id`, `address`, `status_id`, `type_id`, `created_by`, `created_at`) VALUES
(1, 'Title', '<p>sample description</p>', 424000, 1, 'fdasfads', 1, 0, 'syeedmishu', '2017-05-08 11:06:07'),
(2, 'Donation needed for diserster', '<p><em>Discription shuld be <br /></em></p>', 4000, 2, 'Mymensingh', 1, 0, 'syeedmishu', '2017-05-08 11:05:38'),
(3, 'test title', '<p>fdfasdfa</p>', 0, 1, 'fasdfa', 0, 0, 'syeed', '2017-05-15 02:38:32'),
(4, 'demo title', '<p>fdsf</p>', 0, 1, 'fadf', 1, 0, 'syeed', '2017-05-17 10:17:42'),
(6, 'flk;adsflasl', '<p>dsfadsf</p>', 40000, 2, 'fdasfda', 0, 0, 'syeed', '2017-05-17 08:57:29'),
(10, 'fadsfasdf', '<p>fasdfadsf</p>', 0, 3, 'fasdfasdf', 1, 0, 'syeed', '2017-05-17 13:41:16'),
(11, 'fasdfasdf', '<p>fasdfa</p>', 0, 2, 'fasdfadsfa', 0, 0, 'syeed', '2017-05-17 09:46:24'),
(12, 'dfadsf', '<p>fsdfa</p>', 0, 2, 'fdsfasd', 0, 0, 'syeed', '2017-05-17 14:00:25'),
(13, 'fadsffadsf', '<p>fasdfa</p>', 5855555, 2, 'fadsfa', 0, 0, 'syeed', '2017-05-17 14:29:11'),
(14, 'fadsffadsf', '<p>fasdfa</p>', 5855555, 2, 'fadsfa', 0, 0, 'syeed', '2017-05-17 14:30:44'),
(15, 'Post for organi', '<p style=\"text-align: center;\"><em><strong>fafpost for organization</strong></em></p>', 78888888, 2, 'orgfdsfa', 0, 0, 'syeed', '2017-05-18 11:17:15'),
(17, 'new', '<p>des</p>', 900000, 2, 'dfsdaf', 1, 1, 'syeed', '2017-05-19 06:18:45'),
(19, 'this post is for an organization', '<p style=\"text-align: justify; padding-left: 30px;\"><strong>The search will start at the specified position, or at the beginning if no start position is specified, and end the search at the end of the array.</strong></p>', 10000000, 3, 'Dhaka', 1, 2, 'syeed', '2017-05-23 10:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `donation_types`
--

CREATE TABLE `donation_types` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation_types`
--

INSERT INTO `donation_types` (`type_id`, `type_name`) VALUES
(2, 'organization'),
(9, 'personal'),
(15, 'test'),
(17, 'newone');

-- --------------------------------------------------------

--
-- Table structure for table `link_table`
--

CREATE TABLE `link_table` (
  `link_id` int(11) NOT NULL,
  `donation_id` int(11) NOT NULL,
  `link_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `link_table`
--

INSERT INTO `link_table` (`link_id`, `donation_id`, `link_detail`) VALUES
(1, 14, 'link 1'),
(2, 14, 'link2'),
(3, 14, 'link3'),
(4, 15, 'https://bitbucket.org/'),
(5, 15, 'https://bitbucket.org/'),
(6, 16, 'http://localhost/donation-system/create-donation-post'),
(7, 16, 'http://stackoverflow.com/questions/13343566/set-select-option-selected-by-value'),
(9, 17, 'http://localhost/donation-system/post/update/17'),
(10, 19, 'http://stackoverflow.com/questions/25331030/js-get-second-to-last-index-of'),
(11, 19, 'https://www.w3schools.com/jsref/jsref_indexof_array.asp'),
(20, 19, 'http://localhost/donation-system/post/update/19'),
(21, 19, 'https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_split'),
(22, 19, 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf?v=control');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `created_by`, `created_at`) VALUES
(1, 'admin', '', '2017-05-02 12:47:29'),
(2, 'superadmin', '', '2017-05-02 12:47:18'),
(3, 'financemanagement', '', '2017-05-02 12:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `status_table`
--

CREATE TABLE `status_table` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_table`
--

INSERT INTO `status_table` (`status_id`, `status_name`) VALUES
(1, 'active'),
(2, 'inactive');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `provider_id` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `profile_url` text,
  `picture_url` text,
  `status_id` int(11) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `email`, `password`, `provider`, `provider_id`, `role_id`, `gender`, `locale`, `profile_url`, `picture_url`, `status_id`, `created_by`, `created_at`) VALUES
(60, NULL, NULL, 'syeed1', 'syeed@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, NULL, '', NULL, NULL, 1, '', '2017-05-17 08:18:19'),
(74, NULL, NULL, 'syeed', 'syeed007@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, NULL, '', NULL, NULL, 1, '', '2017-05-17 08:18:39'),
(90, NULL, NULL, 'mishusyeed', 'mishusyeed@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, NULL, '', NULL, NULL, 1, 'syeed', '2017-05-23 08:21:23'),
(91, 'Syeed', 'Mishu', NULL, 'abusyeed007@outlook.com', NULL, 'facebook', '1677978225565421', NULL, 'male', 'en_US', 'https://www.facebook.com/1677978225565421', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/16708263_1557054567657788_7083783041784865177_n.jpg?oh=3225fb9e14089e22efe1133e092939cf&oe=59B31FE8', NULL, '', '2017-05-23 02:32:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `donates`
--
ALTER TABLE `donates`
  ADD PRIMARY KEY (`donate_id`);

--
-- Indexes for table `donations`
--
ALTER TABLE `donations`
  ADD PRIMARY KEY (`donation_id`);

--
-- Indexes for table `donation_types`
--
ALTER TABLE `donation_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `link_table`
--
ALTER TABLE `link_table`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `status_table`
--
ALTER TABLE `status_table`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `donates`
--
ALTER TABLE `donates`
  MODIFY `donate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `donations`
--
ALTER TABLE `donations`
  MODIFY `donation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `donation_types`
--
ALTER TABLE `donation_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `link_table`
--
ALTER TABLE `link_table`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_table`
--
ALTER TABLE `status_table`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
