
<div class="container-fluid">
    <div class="col-md-10 col-md-offset-2">

        <form class="">
            <div style="margin-top: 20px" class="col-md-5 col-md-offset-7">
                <input type="text" name="search" id="search-user" class="form-control search" placeholder="Search.."/>
                <ul id="finalResult"></ul>
            </div>
        </form>

        <table style="margin-top: 50px;" class="table table-hover">
            <thead>
            <th>#</th>
            <th>User Name</th>
            <th>E-Mail </th>
            <th>Role</th>
            <th>Status</th>
            <th>Edit</th>
            <th>Delete</th>
            </thead>
            <tbody>
            <?php
            $count = 1;
            foreach ($user_list as $row){

                echo   form_open('user/delete');
                echo "<tr>  
                                
                                <input type='hidden' id='user_id_{$row['user_id']}' name='user_id' value='{$row['user_id']}'>
                                <td>{$count}</td>
                                <td>{$row['username']}</td>
                                <td>{$row['email']}</td>
                                <td>{$row['name']}</td>";

                ?>
                <td>
                    <select  name="status" class="form-control user_status">
                        <?php foreach ($status->result() as $result){
                            echo "<option value='$result->status_id'>$result->name</option>";
                        }?>
                    </select>
                </td>
                <td><input type='button' onclick="location.href='<?=base_url();?>user/update/<?=$row['user_id'];?>'" value='Edit' class='btn btn-info'></td>
                <td><input type='submit' name='submit' class='btn btn-danger' value='Delete'> </td>


                <?php
                echo  "</tr>";
                echo  form_close();
                $count++;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#search-user').on('keyup',function () {
            if($('#search-user').val().length>0){
                console.log('im on');
                $.ajax({
                    method:'post',
                    url:'<?=base_url()?>UserManagementController/search_user',
                    cache:false,
                    data:'search='+$('#search-user').val(),
                    success:function (response) {
                        $('#finalResult').html("");
                        var obj = JSON.parse(response);
                        console.log(obj);
                        if(obj.length>0){
                            var items = [];
                            try{
                                $.each(obj,function (i, val) {
                                    var Li =$( "<li>"+val.username+"</li>");
                                    console.log(Li);
                                    var a = $("<a href='"+BASE_URL+'user/search-result/'+val.user_id+"'></a>");
                                    a.append(Li);
                                    items.push(a);
                                });
                                $('#finalResult').append.apply($('#finalResult'),items);
                            }catch (e){
                                console.log("Error"+e.toString());
                            }
                        }else {
                            $('#finalResult').append("<li/>").text("no data found");
                        }


                    }
                });
            }
        });
    });
</script>