$(document).ready(function(){


    // $(document).ready(function () {
    //     tinymce.init({
    //         selector: 'textarea'
    // });
    //     tinyMCE.activeEditor.getContent();
    // });

//form tab code start
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale('+scale+')'});
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function(){
        return true;
    });

//file uploading code
    var nextIndex = 1;
    function attachFileReader(fileInput)
    {
       //var ext = fileInput.split('.').pop();
        if (window.FileReader) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if( $(fileInput).siblings('.preview').html() != "" ){
                    // $(fileInput).siblings('.preview').html('');
                    // $(fileInput).siblings('.preview').html($('<img></img>').attr('src', e.target.result).width(100));
                    return;
                }

               // var extension = fileInput.substr( (fileInput.lastIndexOf('.') +1) );


                    // $(fileInput).siblings('.preview').append($('<img></img>').attr('src', e.target.result).width(100));
var url = BASE_URL+'Uploaded_files/doc.jpg';
                    $(fileInput).siblings('.preview').append($('<img></img>').attr('src', url).width(100));


                /*and now add one more*/
                //nextIndex = $('#picture input[type="file"]').size() + 1;

                nextIndex = nextIndex + 1;
                var next = $('<div></div>').addClass('one-photo');
                var preview = $('<div></div>').addClass('preview');
                var icon = $('<i></i>').addClass('fa fa-plus');


                var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').addClass('imageUploads').attr('data-index',nextIndex).attr('name', 'photo[]');


                var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function (e)
                {
                    if ($('#picture .one-photo').length > 0) {

                        if ($(this).parent('.one-photo').children('.preview').html() != "") {
                            // if ($('#picture .one-photo .preview').html() != "") {
                            // console.log($(this).parent('.one-photo').children('.preview').html());
                            console.log($(this).tagName);
                            e.preventDefault();
                            next.remove();
                        }

                    }else{
                        e.preventDefault();
                        //$('#imageUploads').val('');
                        //$('#picture').fadeOut(500);
                        // $(".one-photo").hide();
                    }
                });
                next.append(input);
                next.append(removeThisPicture);
                next.append(icon);
                next.append(preview);
                $(fileInput).parent('.one-photo').after(next);
                input.change(function () {
                    attachFileReader(this);
                });
            };
            reader.readAsDataURL($(fileInput) [0].files[0]);
        }
        else
        {
            var next = $('<div></div>').addClass('one-photo');
            var preview = $('<div></div>').addClass('preview');


            var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo[]');


            input.change(function () {
                attachFileReader(this);
            });
            var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function (e)
            {
                if ($('#picture .one-photo').length > 0) {

                    if ($(this).parent('.one-photo').children('.preview').html() != "") {
                        // if ($('#picture .one-photo .preview').html() != "") {
                        // console.log($(this).parent('.one-photo').children('.preview').html());
                        console.log($(this).tagName);
                        e.preventDefault();
                        next.remove();
                    }

                }else{
                    e.preventDefault();
                    $('#imageUploads').val('');
                    $('#picture').fadeOut(500);
                    // $(".one-photo").hide();
                }
            });
            var icon = $('<i></i>').addClass('fa fa-plus');
            next.append(input);
            next.append(removeThisPicture);
            next.append(icon);
            next.append(preview);
            $(fileInput).parent('.one-photo').after(next);
        }
    }

    $(document).ready(function ()
    {
        $('.removePhoto').click(function (e)
        {
            if ($('#picture .one-photo').length > 0) {

                if ($(this).parent('.one-photo').children('.preview').html() != "") {
                    // if ($('#picture .one-photo .preview').html() != "") {
                    // console.log($(this).parent('.one-photo').children('.preview').html());
//                    console.log($(this).tagName);
                    e.preventDefault();
                    //next.remove();
                    $(this).parent('.one-photo').remove();
                }

            }
        });

        $('.fileUpload').change(function () {
            attachFileReader(this);
        });

    });

    $(document).ready(function () {
        $("#donation-amount").hide();
        $("#fixed").click(function () {

            $("#donation-amount").show();
            $("#unlimited").prop('checked', false);
        });
        $("#unlimited").click(function () {
            $("#donation-amount").hide();
            $("#fixed").prop('checked', false);
        });
    });

    $(document).ready(function () {
        tinymce.init({
            selector: 'textarea',
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });

    });
    $('#remove-post').click(function (event) {

    });

    //status check code
    $('#status_check').change(function () {
        var url =BASE_URL;
        var id = $('#donation_id').val();
        var status = $(this).val();
        $.ajax({
            type:"post",
            url:url+"DonationPostController/status_update",
            data:{check:status,donation_id:id},
            success:function (data) {
                console.log(data);
            }
            }
        );
    });





    //search code start form here




});