
//data table for user start.....
var dataTable;
$(document).ready(function () {

    var dataTable = $('#user_data').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
            url:BASE_URL+'UserManagementController/user_data_table',
            type:"POST"
        },
        "columnDefs":[
            {
//                   "targets":[0, 3, 4],
                "orderable":false
            }
        ]
    });


        $('body').on('click','.delete_user',function () {
            console.log("clicked");
            var id = $(this).val();
            var parent = $(this).parent().parent();

            console.log(parent);
            $.ajax({
                method: "post",
                url:BASE_URL+'user/delete',
                data:{id:id},
                success:function (response) {
                    $(parent).remove();
                    dataTable.ajax.reload();
                }
            });
        });


    $(document).on('click','.update_user',function () {
       var user_id  = $(this).attr('id');
       $.ajax({
          url:BASE_URL+'UserManagementController/fetch_single_user',
           method: "post",
           data:{user_id:user_id},
           dataType:"json",
           success:function (data) {
                $('#modal_form').modal('show');
                $('#username').val(data.username);
                $('#email').val(data.email);
                $('#user_id').val(data.user_id);
               $('#role').val(data.role_id);
               $('#status').val(data.status_id);
               // $('#btn_update').val("Edit");

           }
       });
    });

    $(document).on('submit','#user_update_form',function (event) {
        event.preventDefault();
        var username =  $('#username').val();
        var email = $('#email').val();
        var role_id = $('#role').val();
        var status_id =$('#status').val();
        var submit = $('#btn_update').val();
        var hidden=$('#user_id').val();
    console.log(role_id);

            $.ajax({
                url:BASE_URL+'UserManagementController/update_user',
                method:"post",
                data:{username:username,email:email,role_id:role_id,status_id:status_id,submit:submit,hidden:hidden},
                success:function (data) {
                    $('#user_update_form')[0].reset();
                    $('#modal_form').modal('hide');
                    dataTable.ajax.reload();
                }

            });



    });

});

//data table for user end--->


// data table for category start--->

var cat_dataTable;
$(document).ready(function () {
    var cat_dataTable = $('#category_data').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
            url:BASE_URL+'CategoryController/category_data_table',
            type:"POST"
        },
        "columnDefs":[
            {
//                   "targets":[0, 3, 4],
                "orderable":false
            }
        ]
    });


    $('body').on('click','.delete_category',function () {
        console.log("clicked");
        var id = $(this).val();
        var parent = $(this).parent().parent();

        console.log(parent);
        $.ajax({
            method: "post",
            url:BASE_URL+'CategoryController/delete_category',
            data:{id:id},
            success:function (response) {
                $(parent).remove();
                cat_dataTable.ajax.reload();
            }
        });
    });

    $(document).on('click','.update_category',function () {
        var category_id  = $(this).attr('id');
        console.log("clicked");
        $.ajax({
            url:BASE_URL+'CategoryController/fetch_single_user',
            method: "post",
            data:{category_id:category_id},
            dataType:"json",
            success:function (data) {
                $('#modal_form_cat').modal('show');
                $('#category_name').val(data.category_name);
                $('#category_id').val(data.category_id);
                $('#btn_update_cat').val("Edit");

            }
        });
    });

    $(document).on('submit','#category_update_form',function (event) {
        event.preventDefault();
        var category_name =  $('#category_name').val();
        var submit = $('#btn_update_cat').val();
        var hidden=$('#category_id').val();


        $.ajax({
            url:BASE_URL+'CategoryController/update_category',
            method:"post",
            data:{category_name:category_name,submit:submit,hidden:hidden},
            success:function (data) {
                $('#category_update_form')[0].reset();
                $('#modal_form_cat').modal('hide');
                cat_dataTable.ajax.reload();
            }

        });



    });


});

//data table for category end-->


//data table for donation start-->
var donation_dataTable;
$(document).ready(function () {
    var donation_dataTable = $('#donation_data_table').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
            url:BASE_URL+'DonationPostController/donation_data_table',
            type:"POST"
        },
        "columnDefs":[
            {
//                   "targets":[0, 3, 4],
                "orderable":false
            }
        ]
    });

    $(document).on("click",'.view',function () {
       location.href=BASE_URL+'view-post/'+$(this).val();
    });
});

// data table for donation end--->

//data table for donation type start-->


$(document).ready(function () {


});

//document delete section start-->
$(document).ready(function () {
    $(document).on('click','#rmv_doc',function () {
        var id = $(this).parent('div').attr('id');
        $.ajax({
            url:BASE_URL+'DocumentController/delete_document',
            method:"post",
            data:{id:id},
            success:function (data) {

            }
        });

    });
});


//link delete section start->

$(document).ready(function () {
    $('body').on('click','.remove_btn',function () {
       var id = $(this).parent().siblings().children().closest('input').val();
       $.ajax({
           url:BASE_URL+'LinkController/delete_link',
           method:"post",
           data:{id:id},
           success:function () {
               
           }
       })
    });
});

// donation type update code start-->
var donation_type_dataTable;
$(document).ready(function () {
    var donation_type_dataTable = $('#donation_type_data_table').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
            url:BASE_URL+'DonationTypeController/donation_type_data_table',
            type:"POST"
        },
        "columnDefs":[
            {
//                   "targets":[0, 3, 4],
                "orderable":false
            }
        ]
    });

    $(document).on('click','.update_type_val',function () {
        var type_id  = $(this).attr('id');
        console.log("clicked");
        $.ajax({
            url:BASE_URL+'DonationTypeController/fetch_single_type',
            method: "post",
            data:{type_id:type_id},
            dataType:"json",
            success:function (data) {
                $('#modal_form_type').modal('show');
                $('#type_name').val(data.type_name);
                $('#type_id').val(data.type_id);
                $('.btn_update_type').val("Edit");
                $('.btn_update_type').html("Edit");

            }
        });
    });



    $(document).on('click','#edit_btn',function (event) {
        event.preventDefault();
        var type_name =  $('#type_name').val();
        var submit = $('.btn_update_type').val();
        var hidden=$('#type_id').val();
        $.ajax({
            url:BASE_URL+'DonationTypeController/update_type',
            method:"post",
            data:{type_name:type_name,submit:submit,hidden:hidden},
            success:function (data) {
                $('#type_update_form')[0].reset();
                $('#modal_form_type').modal('hide');
                donation_type_dataTable.ajax.reload();
            }

        });

    });

    $('body').on('click','.delete_type',function () {
       var id = $(this).attr('id');
       $.ajax({
           url:BASE_URL+'DonationTypeController/delete_type',
           method:"post",
           data:{id:id},
           success:function (data) {
               donation_type_dataTable.ajax.reload();
           }
       });
    });


   $(document).on('click','#add_type',function () {
       $('#modal_form_type').modal('show');
       $('.add_type').val('Add');
       $('.add_type').html('Add');
        $('form').attr('id','add_type_form');
   });

 $(document).on('click','.add_type',function () {
    var type_name = $('#type_name').val();
    var id = $(this).val();

     $.ajax({
         url:BASE_URL+'DonationTypeController/add_type',
         method:"post",
         data:{type_name:type_name,btn:id},
         success:function (data) {
             $('#add_type_form')[0].reset();
             $('#modal_form_type').modal('hide');
             donation_type_dataTable.ajax.reload();
         }
     });
 });

});

