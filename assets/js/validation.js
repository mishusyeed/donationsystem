// function validation() {
//     if(document.userForm.firstName.value==""){
//         window.alert("Please provide First name.");
//
//         document.userForm.firstName.focus();
//         return false;
//     }
//     if(document.userForm.lastName.value==""){
//         window.alert("Please provide Last name.");
//
//         document.userForm.lastName.focus();
//         return false;
//     }
//     var emailAddress =document.userForm.email.value;
//     var atpos = emailAddress.indexOf("@");
//     var dotpos = emailAddress.lastIndexOf(".");
//     if(atpos < 1 || (dotpos - atpos < 2)){
//         alert("Enter a valid Mail Address");
//         document.userForm.email.focus();
//         return false;
//     }
//     if(document.userForm.userName.value == ""){
//         window.log("Please provide UserName.");
//         document.userForm.userName.focus();
//         return false;
//     }
//     if(document.userForm.password.value == "" || document.userForm.password.value.length < 6){
//         alert("Password should be more 6 digits");
//         document.userForm.password.focus();
//         return false;
//     }
//     if(document.userForm.gender.value.checked){
//         return true;
//     }else {
//         alert("Select Gender");
//         return false;
//     }
//
//     return true;
//
// }

$().ready(function () {


    $("#signupForm").validate({
        rules: {
            firstName: "required",
            lastName: "required",
            userName: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
//                    topic: {
//                        required: "#newsletter:checked",
//                        minlength: 2
//                    },
//                    agree: "required"
        },
        messages: {
            firstName: "Please enter your firstname",
//                    firstname: "Please enter your firstname",
            lastName: "Please enter your lastname",
            userName: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: {
                required: "Please enter an email address",
                email: "please enter a valid email address"
            }

        }
    });
    $("#roleform").validate({
        rules: {
            role_Name:"required",
            slug : "required"
        },
        messages:{
            role_Name:"Please Enter Role Name",
            slug: "Please Enter valid slug"
        }
    });



});