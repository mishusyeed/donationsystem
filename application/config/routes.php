<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'DonationPostUser/donation_post_user_view';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = 'LoginController';
$route['role'] = 'RoleController/index';
$route['category']='CategoryController';
$route['category/create'] = 'CategoryController/create';
$route['donation/upload']='FileUploadController/upload_document';
$route['create-donation-post'] = 'DonationPostController';
$route['view-post/(:any)'] = 'DonationPostController/viewpost/$1';
$route['admin'] = 'UserManagementController/admin';
$route['superadmin'] = 'UserManagementController/super_admin';
$route['financemanagement'] = 'UserManagementController/finance_management';
$route['user/status'] = 'UserManagementController/user_status_update';
$route['logout'] = 'UserManagementController/logout';
$route['post/update/(:any)'] = 'DonationPostController/update_post_data/$1';
$route['post/delete'] = 'DonationPostController/post_delete';
$route['admin/create-user'] = 'LoginController/create_member';
$route['admin/signup'] = 'LoginController/signup';
$route['admin/user_list'] = 'UserManagementController/user_list';
$route['user/delete'] = 'UserManagementController/delete_user';
$route['user/update/(:any)'] = 'UserManagementController/update_user_data/$1';
$route['user/update_data'] ='UserManagementController/update_user';
$route['donation/view'] = 'DonationPostController/post_table';
$route['category/view_category'] = 'CategoryController/show_category_list';
$route['category/update/(:any)'] = 'CategoryController/get_category_for_update/$1';
$route['category/delete'] = 'CategoryController/delete_category';
$route['category/search-result/(:any)'] = 'CategoryController/get_cat_after_search/$1';
$route['user/search-result/(:any)'] = 'UserManagementController/view_searched_user/$1';
$route['donation/type_list'] = 'DonationTypeController/donation_type_list';
$route['donate/(:any)'] = 'DonateController/index/$1';