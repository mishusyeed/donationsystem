<nav class="side-nav">
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul style="background-color: white;" class="nav navbar-nav side-nav">
            <li >
                <a id="user" class="active"  href=""><i class="fa fa-user" aria-hidden="true"></i>
                    </i> User List</a>
            </li>
            <li>
                <a id="role" href=""><i class="fa fa-user" aria-hidden="true"></i> Role List</a>
            </li>

            <li >
                <a  href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-tags" aria-hidden="true"></i>
                    Category <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo3" class="collapse">
                    <li >
                        <a id="c_cat" href="">Create Category </a>
                    </li>
                    <li>
                        <a id="l_cat" href="">Category List</a>
                    </li>
                    <li>
                        <a id="u_cat" href="">Update Category</a>
                    </li>
                    <li>
                        <a id="d_cat" href="">Delete Category</a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
