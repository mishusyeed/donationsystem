
   <nav class="side-nav">
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul style="background-color: white;" class="nav navbar-nav side-nav">

                    <li >
                        <a  href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-user" aria-hidden="true"></i> Donation Post <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                            <li>
                                <a  id="view_post" href="<?=base_url();?>donation/view">View Post</a>

                            </li>
                            <li>
                                <a  id="create_post" href="<?=base_url();?>create-donation-post">Create Post</a>
                            </li>

                        </ul>
                    </li>
                    <li >
                        <a  href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-user" aria-hidden="true"></i> User<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li >
                                <a id="create_user" href="<?=base_url();?>admin/signup">Create User</a>
                            </li>
                            <li>
                                <a id="user_list" href="<?=base_url();?>admin/user_list">User List</a>
                            </li>
                        </ul>
                    </li>



                    <li >
                        <a  href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-user" aria-hidden="true"></i> Category<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                            <li >
                                <a id="create_category" href="<?=base_url();?>category">Create Category </a>
                            </li>
                            <li>
                                <a id="category_list" href="<?=base_url();?>category/view_category">Category List</a>
                            </li>
                        </ul>
                    </li>

                    <li >
                        <a  href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-user" aria-hidden="true"></i> Donation Types<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li >
                                <a id="type_list" href="<?=base_url();?>donation/type_list">Type List </a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>
   </nav>


    <!-- Bootstrap Core JavaScript -->

