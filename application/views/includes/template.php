<?php $this->load->view('includes/header'); ?>
<?php if(isset($nav)){
    $this->load->view('includes/'.$nav);
} ?>
<?php if(isset($sidebar)){
    $this->load->view('includes/'.$sidebar);
} ?>

<?php
if(isset($main_content)){
    $this->load->view($main_content);
}
 ?>
<?php $this->load->view('includes/footer'); ?>
