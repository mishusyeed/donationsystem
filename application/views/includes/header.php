<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?=base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/datepicker.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/tags.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.min.css">
    <script src="<?= base_url(); ?>assets/js/jquery.js"></script>
    <script>
        var BASE_URL = '<?= base_url() ?>';
    </script>
    <title></title>
</head>
<body>
