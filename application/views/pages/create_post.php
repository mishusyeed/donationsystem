<?php
    $category_records = $category->result_array();
?>

<div  class="col-md-offset-2 col-md-10">
    <?=form_open_multipart('DonationPostController/createDonation','class="form-horizontal" id="msform"');?>
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active">Personal Info</li>
            <li>Upload Documents</li>
            <li>Authentic Link</li>
        </ul>
        <fieldset>
            <h2 class="fs-title">Personal Info</h2>

            <select id="category" class="form-control" name="category_id">
                <option value="">-Select Category-</option>
                <?php foreach ($category_records as $record){
                    echo "<option value='".$record["category_id"]."'>".$record['category_name']."</opition>";
                }?>
            </select>
            <br/>
            <select id="type" class="form-control" name="type_id">
                <option value="">-Select Type-</option>
                <?php foreach ($donation_type as $type){
                    echo "<option value='".$type->type_id."'>".$type->type_name."</opition>";
                }?>
            </select>
            <br/>
            <input class="form-control" type="text" id="title" name="title" placeholder="Title" required/>
            <div class="row">
            <div  class="col-md-2 radio">
                <label><input type="radio"  id="unlimited" style="margin-left:0px; margin-right: 0px;"  name="type" value="unlimited" checked="checked">Unlimited</label>
            </div>
            <div class="col-md-2 radio">
                <label><input type="radio"  id="fixed"  value="fixed" name="type1">Fixed</label>
            </div>
            </div>
            <input class="form-control" type="text" id="donation-amount" name="donation-amount" placeholder="Donation Amount" />
            <textarea name="description" class="description" id="description" rows="5" placeholder="Description" required></textarea>
            <input style="margin-top:5px;" class="form-control" type="text" id="address" name="address" placeholder="Address" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Please upload Documents</h2>

            <div class="form-group">
                <div class="col-md-9">
                    <div id="picture">
                        <div style="display:table;">
                            <div class="one-photo">
                                <input type="file" id="photo" class="fileUpload upload"  name="photo[]" multiple/>
                                <div class="removePhoto">x</div>
                                <i class="fa fa-plus" style="margin-top:25px;"></i>
                                <div class="preview"></div>
                            </div>

                        </div>

                        <div class="clear"></div>

                    </div>

                </div>
            </div>

            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Please Provide An authentic link</h2>
            <div class="container-class">
            <div class="row ">
                <div  class="col-md-11 ">
                    <input class="form-control " type="text" name="link_field[]" placeholder="Link" />
                </div>
                <div class="col-md-1">
                    <button type="button" id="add_link" class=" btn btn-success">+</button>
                </div>


            </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" name="submit" class="submit action-button" value="Submit" />
        </fieldset>
    <?=form_close();?>

</div>
<script>
    $(document).ready(function(){
        var counter = 0;

        $('#add_link').click(function(){

            counter += 1;

            $('.container-class').append(

            '<div class="row">'+'<div class="col-md-11">'+
            '<input placeholder="link.." class="form-control" id="field_' + counter + '" name="link_field[]' + '" type="text" />'+'</div>'+'<div class="col-md-1">'+'<button class="btn btn-danger remove_btn">'+'<i class="fa fa-times" aria-hidden="true"></i>'+
            '</button>'+'</div>'+'</div>'
            );

        });
        $("body").on('click','.remove_btn',function () {
            $(this).closest('.row').remove();
        });


    });
</script>