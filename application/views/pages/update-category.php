
<?php
    $updating_row = $records->row_array();

?>
<div class="col-md-offset-2 col-md-8">
    <?=form_open('CategoryController/update_category','class="form-horizontal"' );?>
    <label for="Name">Category Name</label>
    <input type="text" name="name" value="<?php echo $updating_row['name'];?>" class="form-control">
    <input type="hidden" name="hidden" value="<?php echo $updating_row['category_id'];?>">
    <br>
    <input type="submit" name="submit" value="Edit" class="btn btn-info">
    <?=form_close();?>

</div>
