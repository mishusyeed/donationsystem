
<div class="container-fluid">
    <div class="col-md-10 col-md-offset-2">

<table style="margin-top: 50px;" class="table table-hover">
    <thead>
    <th>#</th>
    <th>User Name</th>
    <th>E-Mail </th>
    <th>Role</th>
    <th>Edit</th>
    <th>Delete</th>
    </thead>
    <tbody>
    <?php
    $count = 1;
    foreach ($records as $row){

        echo   form_open('user/delete');
        echo "<tr>  
                                
                                <input type='hidden' id='user_id_{$row['user_id']}' name='user_id' value='{$row['user_id']}'>
                                <td>{$count}</td>
                                <td>{$row['username']}</td>
                                <td>{$row['email']}</td>
                                ";

        ?>
        <td>
            <select  name="status" class="form-control user_status">
                <?php foreach ($status->result() as $result){
                    echo "<option value='$result->status_id'>$result->name</option>";
                }?>
            </select>
        </td>
        <td><input type='button' onclick="location.href='<?=base_url();?>user/update/<?=$row['user_id'];?>'" value='Edit' class='btn btn-info'></td>
        <td><input type='submit' name='submit' class='btn btn-danger' value='Delete'> </td>


        <?php
        echo  "</tr>";
        echo  form_close();
        $count++;
    }
    ?>
    </tbody>
</table>
    </div>
</div>