<div  class="container-fluid">
    <div style="margin-top: 50px;" class="col-md-10 col-md-offset-2">

<button id="add_type"  style="margin-bottom: 10px;" type="button" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>
</button><br/>

        <table id="donation_type_data_table" style="margin-top: 50px;" class="table table-hover">
            <thead>
            <th>#</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modal_form_type" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Donation Type Form</h3>
            </div>
            <div class="modal-body form">
                <form action="" id="type_update_form" class="form-horizontal">

                    <div class="form-body">
                        <input type="hidden" id="type_id" name="hidden"/>

                        <div class="form-group">
                            <label class="control-label col-md-3">Type Name</label>
                            <div class="col-md-9">
                                <input name="type_name" id="type_name" placeholder="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button name="submit" id="edit_btn" type="button"  class="btn btn-primary btn_update_type add_type">Edit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


