<div style="margin-top: 65px;" class="col-md-offset-2 post_outer">
    <?php
        foreach ($records->result() as $row){ ?>

        <div class="post">
            <div class="inner_post">
<!--                <form action="#">-->



                <div id="remove-post" onclick="location.href='<?=base_url();?>donation/view'" style="text-align: right;" class="col-md-offset-11"><i class="fa fa-times" aria-hidden="true"></i>
                </div>
            <?=form_open('post/delete');?>
                <input type="hidden" id="donation_id" name="id" value="<?=$row->donation_id;?>">
                <div class="col-md-2 col-md-offset-10">
                <label for="status">Status</label>
                <select id="status_check" name="status" class="form-control">
                    <?php foreach ($status->result() as $stat){

                        echo "<option value='{$stat->status_id}'>{$stat->status_name}</option>";
                    }?>

                </select>
                </div>

            <h2>Title:<?php echo $row->title;?> </h2><br>
            <p>Description: <?php echo $row->description;?></p>
            <P> <?php
                if($row->donation_amount >0 ){
                    echo '<b>Amount Needed:</b>'.$row->donation_amount;
                }else{
                    echo "<b>Donation amount:</b> Unlimited";
                }
                ?></P>
                <P><b>Post time:</b> <?php echo $row->created_at;?></P>
                <div class="container-fluid"><div style="margin:20px 0px;"><h3>Documents:</h3></div>

                        <?php foreach ($documents as $file){
                    ?>
                            <?php if($file->type === 'image/jpeg' || $file->type === 'image/jpg' || $file->type == 'image/png'){
                                ?>
                                <div class="col-md-3">
                                <img class="img-class" style='width: 100%; height: 100%;' src='<?=base_url().$file->name;?>'/>
                                </div>

                           <?php } else if($file->type ==='application/pdf'){ ?>
                                <object data="<?=$file->name?>" type="application/pdf" width="250px" height="250px">
                                    <div class="col-md-3"><a href="<?=base_url().$file->name;?>"><img class="img-class" style='width:100%; height: 100%;' src='<?=base_url().'Uploaded_files/doc.jpg';?>'/></a></div>
                                </object>

<!--                                <img class="img-class" style='width: 250px; height: 250px;' src='--><?//=base_url().'Uploaded_files/doc.jpg';?><!--'/>-->
                                <?php }else{ ?>
                                <div class="col-md-3">
                                    <a  href="<?=base_url().$file->name;?>"><img class="img-class" style='width:100%; height: 100%;' src='<?=base_url().'Uploaded_files/doc.jpg';?>'/></a>

                                </div>

                                <?php } ?>
                    <?php } ?>

                        </div>
                <div id="donation_amount">
                    <input type="text" class="form-control" placeholder="Please enter amount to donate" />
                </div>
                <br/>
                <br/>
                <button type="button" class="btn btn-info col-md-offset-9" onclick="location.href = '<?=base_url();?>post/update/<?=$row->donation_id; ?>';">Edit</button>
                <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                <button type="button" class="btn btn-info" id="donate">Donate</button>
            <?=form_close(); ?>

            </div>
        </div>
        <?php } ?>
</div>


<script>
    $('#donation_amount').hide();
    $(document).on('click','#donate',function () {
        $('#donation_amount').show();
        console.log($('#donation_amount').val());
    });
</script>
