<?php
    $updating_row = $records->row();

    $category_records = $category->result();
    $val = 0;
?>


<div class="col-md-offset-2 col-md-10">
  <?=form_open_multipart('DonationPostController/edit_post','class="form-horizontal" id="msform"' );?>

    <ul id="progressbar">
        <li class="active">Personal Info</li>
        <li>Upload Documents</li>
        <li>Authentic Link</li>
    </ul>
    <fieldset>
        <h2 class="fs-title">Personal Info</h2>
        <label for="category">Select Category:</label>
        <select id="cat_id" class="form-control" name="category_id">
            <?php foreach ($category_records as $record){
                echo "<option value='".$record->category_id."'>".$record->category_name."</opition>";
            }?>
        </select>
        <br/>
        <label for="type">Select Type:</label>
        <select id="type" class="form-control" name="type_id">
            <option value="">-Select Type-</option>
            <?php foreach ($donation_type as $type){
                echo "<option value='".$type->type_id."'>".$type->type_name."</opition>";
            }?>
        </select>
        <br/>
        <label for="title">Title:</label>
        <input type="text" class="form-control" value="<?=$updating_row->title?>" id="title" name="title" placeholder="Title" required/>
        <br/>
        <label for="donation_amount">Donation Amount:</label>
        <input type="text" id="donate" value="<?=$updating_row->donation_amount?>" class="form-control" name="donation-amount" placeholder="Donation Amount" />
        <label for="description">Description:</label>
        <textarea name="description"  class="description" id="description" rows="5" placeholder="Description" required><?=$updating_row->description?></textarea>
        <br/>
        <label for="Address">Address:</label>
        <input style="margin-top: 5px;" class="form-control" value="<?=$updating_row->address?>" type="text" id="address" name="address" placeholder="Address" />
        <input type="button" name="next" class="next action-button" value="Next" />
    </fieldset>
    <fieldset>
        <h2 class="fs-title">Please upload Documents</h2>

        <div class="form-group">
            <div class="col-md-9">
                <div id="picture">
                    <div style="display:table;">
                        <?php
                        $count = 0;
                        foreach ($documents as $doc){
                            $count++;
                        ?>
                        <div id="<?=$doc->document_id?>" class="one-photo">
                            <input type="file" id="photo" class="fileUpload upload"  name="photo[]" multiple/>
                            <div id="rmv_doc" class="removePhoto ">x</div>
                            <i class="fa fa-plus" style="margin-top:25px;"></i>
                            <div id="up_img_<?=$count?>" class="preview">
                                    <img id="" src="<?=base_url().$doc->name?>" style="width: 100px;" />
                                </div>
                        </div>
                        <?php  } ?>
                        <div class="one-photo">
                            <input type="file" id="photo" class="fileUpload upload"  name="photo[]" multiple/>
                            <div class="removePhoto">x</div>
                            <i class="fa fa-plus" style="margin-top:25px;"></i>
                                <div class="preview"></div>
                        </div>

                    </div>


                    <div class="clear"></div>

                </div>

            </div>
        </div>

        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
    </fieldset>
    <fieldset>
        <h2 class="fs-title">Please Provide An authentic link</h2>

        <div class="container-class">

            <div class="row ">
                <div  class="col-md-11 ">

                    <input class="form-control " type="text" name="link_field[]" placeholder="Link" />
                    <input type="hidden" name="link_id[]" value=""/>
                </div>
                <div class="col-md-1">
                    <button type="button" id="add_link" class=" btn btn-success">+</button>
                </div>
            </div>
            <?php
            $count = 0;

            foreach ($link as $l_list){
                $count++;
                ?>

                <div class="row ">
                <div  class="col-md-11 ">
                    <input type="hidden" name="link_id[]" value="<?=$l_list->link_id?>"/>
                    <input class="form-control" id="field_'.<?=$count?>.'" type="text" name="link_field[]" placeholder="Link" value="<?=$l_list->link_detail?>" />
                </div>
                <div class="col-md-1">
                    <button class="btn btn-danger remove_btn"><i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
            </div>
         <?php   } ?>
        </div>
        <input type="hidden" name="hidden" value="<?php echo $updating_row->donation_id;?>">
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="submit" name="submit" class="submit action-button" value="Update" />
    </fieldset>

    <br>
<!--    <input type="submit" name="submit" value="Edit" class="btn btn-info">-->
<?=form_close();?>

</div>

<script>
    $(document).ready(function(){
        var counter = 0;
        var value = 0;

        $('#add_link').click(function(){

            counter += 1;

            $('.container-class').append(

                '<div class="row">'+'<div class="col-md-11">'+
                    '<input type="hidden" name="link_id[]" value=""/>'+
                '<input placeholder="link.." value="" class="form-control" id="field_' + counter + '" name="link_field[]' + '" type="text" />'+'</div>'+'<div class="col-md-1">'+'<button class="btn btn-danger remove_btn">'+'<i class="fa fa-times" aria-hidden="true"></i>'+
                '</button>'+'</div>'+'</div>'
            );

        });
        $("body").on('click','.remove_btn',function () {
            $(this).closest('.row').remove();
        });
        var type_val = '<?=$updating_row->type_id?>';
        var cat_val = '<?=$updating_row->category_id?>';
        console.log(type_val);
$('select#type').val(type_val);
$('select#cat_id').val(cat_val);

    });
</script>