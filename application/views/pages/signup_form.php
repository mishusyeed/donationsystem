<h1 class="col-md-offset-4 col-md-4">Create Account</h1>
<div id="logn_form" class="col-md-offset-4 col-md-4">
    <?php
        echo form_open('login/create_member','class="form-horizontal"');
        ?>
       <label  for='sel1'>Select role:</label>
<?php
        echo "<select class='form-control' name='role_id'>";

                        foreach ($roles->result_array() as $result){

                            echo "<option value='".$result["role_id"]."'>".$result["name"]."</option>";
                        }
        echo "</select>";
        echo "<br/>";
        echo form_input('username','','class ="form-control" placeholder="UserName"');
        echo "<br/>";
//        echo "<label for=\"email\" >E-mail Address</label>";
        echo form_input('email','','class ="form-control" placeholder="E-mail Address"');
        echo "<br/>";
//        echo "<label for=\"Password\" >Password</label>";
        echo form_password('password','','class="form-control" placeholder="Password"');
        echo "<br/>";
        echo form_password('cpassword','','class="form-control" placeholder="Confirm Password"');
        echo "<br/>";
        echo form_submit('submit','SignUp','class="btn btn-default" id="login-btn"');

        echo form_close();
    ?>
    <?php validation_errors('<p class="error">'); ?>
</div>