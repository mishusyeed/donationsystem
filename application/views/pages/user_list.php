
<div class="container-fluid">
    <div style="margin-top: 30px;" class="col-md-10 col-md-offset-2">

<!--        <form class="">-->
<!--            <div style="margin-top: 20px" class="col-md-5 col-md-offset-7">-->
<!--                <input type="text" name="search" id="search-user" class="form-control search" placeholder="Search.."/>-->
<!--                <ul id="finalResult"></ul>-->
<!--            </div>-->
<!--        </form>-->

        <table id="user_data" style="margin-top: 50px;" class="table table-hover">
            <thead>
            <th>#</th>
            <th>User Name</th>
            <th>E-Mail </th>
            <th>Role </th>
            <th>Status </th>
            <th>Edit</th>
            <th>Delete</th>
            </thead>

        </table>
    </div>
</div>


<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">User Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="user_update_form" class="form-horizontal">

                    <div class="form-body">
                        <input type="hidden" id="user_id" name="hidden"/>
                        <div class="form-group">
                            <label class="control-label col-md-3">Role</label>
                            <div class="col-md-9">
                                <select id="role" name="role" class="form-control">
                                    <option value="">--Select Roles--</option>
                                    <?php foreach ($roles as $role){
                                        var_dump($role);
                                        echo "<option value='".$role->role_id."'>$role->name</option>";
                                    }?>

                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9">
                                <input name="username" id="username" placeholder="First Name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">E-Mail</label>
                            <div class="col-md-9">
                                <input name="email" id="email" placeholder="E-mail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select id="status" name="status" class="form-control">
                                    <option value="">--Select Status--</option>
                                    <?php foreach ($status as $sts){
                                      echo "<option value='".$sts->status_id."'>$sts->status_name</option>";
                                      var_dump($sts);
                                    }?>

                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label class="control-label col-md-3">Address</label>-->
<!--                            <div class="col-md-9">-->
<!--                                <textarea name="address" placeholder="Address" class="form-control"></textarea>-->
<!--                                <span class="help-block"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <label class="control-label col-md-3">Date of Birth</label>-->
<!--                            <div class="col-md-9">-->
<!--                                <input name="dob" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">-->
<!--                                <span class="help-block"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                        <div class="modal-footer">
                            <input name="submit" type="submit" id="btn_update" value="Edit" class="btn btn-primary">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
    $(document).ready(function(){



    });
</script>