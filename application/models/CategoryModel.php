<?php
class CategoryModel extends CI_Model{
    public function get_records(){
        $query = $this->db->get('categories');
        return $query;
    }
    public function add_records($table,$data){
        $result= $this->db->insert($table,$data);
        return $result;
    }
    function get_update_data($id){
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('category_id',$id);
        $query = $this->db->get();
       return $query;
    }
    function update_category($id,$data){
        $this->db->where('category_id', $id);
        $result =  $this->db->update('categories', $data);
        return $result;
    }
    function delete_category($id){
        $this->db->where('category_id',$id);
       $result =  $this->db->delete('categories');
       return $result;
    }
    function search_category($search){
        $this->db->select("*");
        $this->db->like('name',$search);
        $this->db->limit(3);
        $query = $this->db->get('categories');
        return $query->result();
    }
    function get_cat_by_id($id){
        $this->db->select("*");
        $this->db->where('category_id',$id);
        $query = $this->db->get("categories");
        return $query->result();
    }


    var $table = "categories";
    var $select_column = array("category_id", "category_name");
    var $order_column = array(null, "category_name", null, null);
    function make_query()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if(isset($_POST["search"]["value"]))
        {
            $this->db->like("category_name", $_POST["search"]["value"]);
        }
        if(isset($_POST["order"]))
        {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('category_id', 'DESC');
        }
    }
    function make_datatables(){
        $this->make_query();
        //$this->get_status();
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function fetch_single_user($id){
        $this->db->where('category_id',$id);
        $query= $this->db->get('categories');
        return $query->result();
    }}
?>