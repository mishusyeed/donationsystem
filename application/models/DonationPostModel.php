<?php
class DonationPostModel extends CI_Model{
    function createDonation($table,$data){
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function show_post_table(){
        $this->db->select('*');
        $this->db->from('donations');
        $this->db->order_by('donation_id','desc');
        $query = $this->db->get();
        return $query;
    }

    function showpost($id){
        $this->db->select('*');
        $this->db->from('donations');
        $this->db->where('donation_id',$id);
        $this->db->order_by('donation_id','desc');
        $query = $this->db->get();
        return $query;
    }
    function show_document($id){
        $this->db->select('*');
        $this->db->from('documents');
        $this->db->where('donation_id',$id);
        $query = $this->db->get();
        $result= $query->result();
        return $result;
    }

    function get_data_for_update($id){
        $this->db->select('*');
        $this->db->from('donations d');
        $this->db->where('donation_id',$id);
        $query = $this->db->get();
        return $query;
    }

    function update_post($id,$data){
        $this->db->where('donation_id', $id);
        $this->db->update('donations', $data);
    }

    function delete($id){
        $this->db->where('donation_id',$id);
        $this->db->delete('donations');
        $this->db->where('donation_id',$id);
        $this->db->delete('documents');
        $this->db->where('donation_id',$id);
     $result =  $this->db->delete('link_table');
     return $result;

    }

    function get_status(){
        $this->db->select('*');
        $this->db->from('status_table');
        $query = $this->db->get();
        return $query;
    }

    function update_status($id,$data){
        $this->db->where('donation_id',$id);
        $result =  $this->db->update('donations',$data);
        return $result;
    }

//    function search_title($q){
//        $this->db->select('*');
//        $this->db->like('title',$q);
//        $this->db->limit(5);
//        $query = $this->db->get('donations');
//        return $query->result();
//    }

    var $table = "donations";
    var $select_column = array("title", "description", "donation_amount","created_at");
    var $order_column = array(null, "username", "email", null, null);
    function make_query()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if(isset($_POST["search"]["value"]))
        {
            $this->db->like("title", $_POST["search"]["value"]);
            $this->db->or_like("donation_amount", $_POST["search"]["value"]);
        }
        if(isset($_POST["order"]))
        {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('donation_id', 'DESC');
        }
    }
    function make_datatables(){
        $this->make_query();
        //$this->get_status();
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function insert_link($data){
        $this->db->insert('link_table',$data);
        return;
    }
    function get_all_link_by_id($id){
        $this->db->select('*');
        $this->db->from('link_table');
        $this->db->where('donation_id',$id);
        $query= $this->db->get();
        return $query->result();
    }
    function get_link($data){
        $this->db->select('*');
        $this->db->from('link_table');
        $this->db->where('link_id',$data);
        $query= $this->db->get();
        return $query->row_array();
    }
    function update_link($id,$data){
        $this->db->where('link_id',$id);
        $this->db->update('link_table',$data);
        return;
    }
    function donation_post_user_view(){
        $query = $this->db->select('*')
            ->from('donations')
            ->join('status_table','donations.status_id= status_table.status_id')
            ->get();
        return $query->result();

    }

}