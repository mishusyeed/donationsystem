<?php
class DonationTypeModel extends CI_Model{
    function get_donation_type(){
        $this->db->select('*');
        $this->db->from('donation_types');
        $query= $this->db->get();
        return $query->result();
    }

    var $table = "donation_types";
    var $select_column = array("type_name",'type_id');
    var $order_column = array(null, "type_name", null, null);
    function make_query()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if(isset($_POST["search"]["value"]))
        {
            $this->db->like("type_name", $_POST["search"]["value"]);
        }
        if(isset($_POST["order"]))
        {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('type_id', 'DESC');
        }
    }
    function make_datatables(){
        $this->make_query();
        //$this->get_status();
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function fetch_single_type($id){
        $this->db->select('*');
        $this->db->from('donation_types');
        $this->db->where('type_id',$id);
        $query= $this->db->get();
        return $query->row();

    }
    function inset_type($data){
        $this->db->insert('donation_types',$data);
    }
    function update_type($id, $data){
        $this->db->where('type_id',$id);
        $this->db->update('donation_types',$data);
        return;
    }
    function delete_type($id){
        $this->db->where('type_id',$id);
        $this->db->delete('donation_types');
        return;
    }
}
