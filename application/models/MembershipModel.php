<?php
class MembershipModel extends CI_Model{

    public function validate(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password',md5($this->input->post('password')));
        $query = $this->db->get();
        $result = $query->row();
       return $result;

    }
    function create_member(){
        $insert_data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password'=>md5($this->input->post('password')),
            'role_id'=>$this->input->post('role_id'),
            'status_id'=>1,
            'created_by' => $_SESSION['username'],
            'created_at' => date('Y-m-d H:i:s')
        );
       $inset= $this->db->insert('users',$insert_data);
        return $inset;
    }
    function get_role($id){
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('role_id',$id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function get_member_list(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('roles','users.role_id = roles.role_id');
        $query = $this->db->get();
        return $query;

    }
    function delete_user($id){
        $this->db->where('user_id',$id);
        $this->db->delete('users');
    }
    function get_data_user_update($id){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_id',$id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function get_all_roles(){
         $this->db->select('*');
         $this->db->from('roles');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function update_user($id,$data){
        $this->db->where('user_id',$id);
        $this->db->update('users',$data);
        return;
    }
    function update_user_status($id, $data){
        $this->db->where("user_id",$id);
        $this->db->update('users',$data);
        return;
    }
    function get_user_status(){
        $this->db->select('*');
        $this->db->from('status_table');
        $query = $this->db->get();
        return $query->result();

    }
//    function search_user($data){
//        $this->db->select('*');
//        $this->db->like('username',$data);
//        $query = $this->db->get('users');
//        return $query->result();
//    }
//    function view_searched_user($id){
//        $this->db->select('*');
//        $this->db->where('user_id',$id);
//        $query = $this->db->get('users');
//        return $query->result_array();
//    }


//data table code

    var $table = "users";
    var $join_table = 'roles';
    var $select_column = array("user_id", "username", "email");
    var $order_column = array(null, "username", "email", null, null);
    function get_status($id){
        $this->db->select('*');
        $this->db->from('status_table');
        $this->db->where('status_id',$id);
        $query  = $this->db->get();
        return $query->row();
    }
    function make_query()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('roles','users.role_id = roles.role_id');
        if(isset($_POST["search"]["value"]))
        {
            $this->db->like("username", $_POST["search"]["value"]);
            $this->db->or_like("email", $_POST["search"]["value"]);
        }
        if(isset($_POST["order"]))
        {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('user_id', 'DESC');
        }
    }
    function make_datatables(){
        $this->make_query();
        //$this->get_status();
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data(){
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function fetch_single_user($user_id){
        $this->db->where('user_id',$user_id);
        $query= $this->db->get('users');
        return $query->result();
    }


}