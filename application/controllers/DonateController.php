<?php
require_once 'BaseController.php';
class DonateController extends BaseController
{
    function __construct()
    {
        parent::__construct();
        $this->is_log_in();
        $this->load->library('session');
        $this->load->model('DonateModel');
    }

    function index()
    {
        $data['user_id'] = $_SESSION['user_id'];
        $data['id'] = $this->uri->segment(2);
        $data['main_content'] = 'pages/donate_page';
        $this->load->view('includes/template', $data);
    }

    function donate_on_post()
    {
if($this->input->post('amount')>0 && !empty($this->input->post('amount'))){
    $data = array(
        'user_id' =>$this->input->post('user_id'),
        'donation_id' =>$this->input->post('id'),
        'donating_amount' =>$this->input->post('amount'),
        'donation_time' =>date("Y-m-d H:i:s")
    );
    $this->DonateModel->insert_donate($data);
}


    }


}