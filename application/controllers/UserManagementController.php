<?php
require_once 'BaseController.php';
class UserManagementController extends BaseController {
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('MembershipModel');
        $this->load->model('DonationPostModel');

    }

    function super_admin(){
        $data['main_content'] ='pages/super_admin_home';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);
    }

    function admin(){
        $data['main_content'] ='pages/admin_home';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'admin_sidebar';
        $this->load->view('includes/template',$data);

    }

    function finance_management(){

    }

    function user_list(){
        $data['roles']=$this->MembershipModel->get_all_roles();
        $data['status'] =$this->MembershipModel->get_user_status();
        //var_dump($data['status']);

        $data['main_content'] ='pages/user_list';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);

    }
    function delete_user(){

            $id = $this->input->post('id');
            $this->MembershipModel->delete_user($id);
//            redirect('admin/user_list');
        return;


    }

    function update_user_data($id){
        $id = $this->uri->segment(3);
        $data['user'] =$this->MembershipModel->get_data_user_update($id);
        $data['roles'] = $this->MembershipModel->get_all_roles();
        $data['main_content'] ='pages/update_user';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);
    }
    function update_user(){

        if($this->input->post('submit')=='Edit'){
            $id = $this->input->post('hidden');

            $data = array(
                'role_id' =>$this->input->post('role_id'),
                'username' => $this->input->post('username'),
                'email' =>$this->input->post('email'),
                'status_id'=>$this->input->post('status_id')
            );
            $this->MembershipModel->update_user($id,$data);
            //echo "updated";

        }
        //echo "updated";
    }
    function user_status_update(){
        $id = $this->input->post('id');
            $data = array(
                'status_id' =>$this->input->post('status')
            );
        $this->MembershipModel->update_user_status($id,$data);
        echo "updated";
    }
//    function search_user(){
//       $data = $this->input->post('search');
//        $result = $this->Membership_model->search_user($data);
//        echo json_encode($result);
//    }
//    function view_searched_user(){
//    $id = $this->uri->segment(3);
//    $data ['records'] = $this->Membership_model->view_searched_user($id);
//        $data['status'] = $this->DonationPost_Model->get_status();
//        $data['main_content'] ='pages/show_search_result_user';
//        $data['nav'] = 'admin_navigation';
//        $data['sidebar'] = 'super_admin_sidebar';
//        $this->load->view('includes/template',$data);
//
//    }

    function user_data_table(){
        $fetch_data = $this->MembershipModel->make_datatables();


//        $status = $this->Membership_model->get_user_status();
        $data = array();
        $count = null;

        foreach($fetch_data as $row)
        {
            $status_check = $this->MembershipModel->get_status($row->status_id);
//            $select = '<select  name="status" class="form-control user_status">';
//            foreach ($status as $sts) {
//                $select.='<option  value="'.$sts->status_id.'">'.$sts->name.'</option>';
//            }
//            $select.='</select>';
                //echo form_open('user/delete');
                $count++;
                $sub_array = array();
                $sub_array[] = $count;
                $sub_array[] = $row->username;
                $sub_array[] = $row->email;
                $sub_array[] = $row->name;
                $sub_array[]=$status_check->status_name;
//                $sub_array[] =$select;
                $sub_array[] = '<button type="button" name="update" id="' . $row->user_id . '" class="btn btn-info btn-xs update_user"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
                $sub_array[] = '<button type="button"  name="delete"  value="' . $row->user_id . '" class="btn btn-danger btn-xs delete_user"/><i class="fa fa-times" aria-hidden="true"></i></button>';

            $data[] = $sub_array;
            //echo  form_close();
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->MembershipModel->get_all_data(),
            "recordsFiltered"     =>     $this->MembershipModel->get_filtered_data(),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function fetch_single_user(){
        $output =array();
       $result =  $this->MembershipModel->fetch_single_user($_POST['user_id']);
       foreach ($result as $row){
           $output['username'] = $row->username;
           $output['email'] = $row->email;
           $output['user_id'] = $row->user_id;
           $output['role_id']= $row->role_id;
           $output['status_id'] = $row->status_id;
       }
       echo json_encode($output);
    }



}


