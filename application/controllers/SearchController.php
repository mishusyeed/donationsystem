<?php
class SearchController extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Membership_model');
        $this->load->model('category_model');
        $this->load->model('DonationPost_Model');
    }
    function index(){

    }

    function search(){
//        $found_result = [];
       $search =  $this->input->post('search');
       $result_1 = $this->Membership_model->search_user($search);
       $result_2 = $this->DonationPost_Model->search_title($search);
       $result_3  = $this->category_model->search_category($search);
       $result = array_merge($result_1,$result_2,$result_3);
       echo json_encode($result);

    }
}