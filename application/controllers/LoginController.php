<?php
//if (!defined('BASEPATH'))
//    exit('No direct script access allowed');
class LoginController extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
$this->load->model('MembershipModel');

    }

    public function index(){

        if(isset($_SESSION['username'])){
            $id = $_SESSION['role_id'];
            $role = $this->MembershipModel->get_role($id);
            $role_name = trim($role->name);
            if ($role_name == 'admin') {
                redirect('admin');
            } elseif ($role_name == 'superadmin') {
                redirect('superadmin');
            } elseif ($role_name == 'financemanagement') {
                redirect('financemanagement');
            }
        }else{
            $data['main_content'] = 'pages/login_form';
            $data['data'] = 'login_form';
            $this->load->view('includes/template',$data);
            $this->load->model('MembershipModel');
            $this->load->model('RoleModel');
        }


    }
public function validate_credentials(){

       $query =  $this->MembershipModel->validate();
       if($query != null) {

           $data = array(
               'user_id'=>$query->user_id,
               'username' => $this->input->post('username'),
               'email' => $query->email,
               'role_id' => $query->role_id,
               'is_log_in' => true

           );


           $this->session->set_userdata($data);
           $id = $_SESSION['role_id'];
           $role = $this->MembershipModel->get_role($id);
           $role_name = trim($role->name);
           if ($role_name == 'admin') {
               redirect('admin');
           } elseif ($role_name == 'superadmin') {
               redirect('superadmin');
           } elseif ($role_name == 'financemanagement') {
               redirect('financemanagement');
           } else {
               redirect('login');
           }


       }
    }
    public function signup(){
        $data['main_content'] = 'pages/signup_form';
        $data['roles'] = $this->RoleModel->view_role();
        $data['nav'] ='admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);
    }

    public function create_member(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Username','trim|required|min_length[4]');
        $this->form_validation->set_rules('email','E-mail','trim|required|valid_email');
        $this->form_validation->set_rules('password','password','trim|required|min_length[5]');
        $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'trim|required|matches[password]');
        if($this->form_validation->run() == false){
            redirect('login/signup');
        }else{

            if($query = $this->MembershipModel->create_member()){
                redirect('admin/user_list');
            }else{
                redirect('admin/signup');
            }
        }
    }

}