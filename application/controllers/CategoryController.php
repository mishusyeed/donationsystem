<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'BaseController.php';
class CategoryController extends BaseController {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('CategoryModel');
        $this->load->database();
    }

    public function index(){
        $data = array();
        //$this->load->model('category_model');

       if($query = $this->CategoryModel->get_records()){
           $data ['records'] = $query;

       }

        $data['category'] = $this->CategoryModel->get_records();
        $data['main_content']='pages/CreateCategory';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);


    }
    public function create(){

        $data = array(
            'category_name' => $this->input->post('name'),
            'created_by' => $_SESSION['username'],
            'created_at' => date('Y-m-d H:i:s')

        );
        $table ='categories';
        $this->CategoryModel->add_records($table,$data);
        redirect('category/view');

    }
    function show_category_list(){
      // $data['records'] = $this->category_model->get_records();
        $data['main_content']='pages/show_category';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);

    }
    function get_category_for_update(){
        $id = $this->uri->segment(3);
        $data['nav'] ='admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $data['main_content'] ='pages/update-category';
        $data['records'] = $this->CategoryModel->get_update_data($id);
        $this->load->view('includes/template',$data);
    }
    function update_category(){
        if($this->input->post('submit')=="Edit"){
            $id = $this->input->post('hidden');
            $data = array(
                'category_name' =>$this->input->post('category_name'),

            );
            $this->CategoryModel->update_category($id,$data);

        }
    }
    function delete_category(){

            $id = $this->input->post('id');
            $this->CategoryModel->delete_category($id);



    }
//    function search_category(){
//        $search = $this->input->post('search');
//        $result = $this->category_model->search_category($search);
//        echo json_encode($result);
//    }
//    function get_cat_after_search(){
//        $id = $this->uri->segment(3);
//        $result = $this->category_model->get_cat_by_id($id);
//        $data['records'] = $result;
//        $data['nav'] ='admin_navigation';
//        $data['sidebar'] = 'super_admin_sidebar';
//        $data['main_content'] ='pages/show_search_result_cat';
//        $this->load->view('includes/template',$data);
//    }


    function category_data_table(){
        $fetch_data = $this->CategoryModel->make_datatables();


//        $status = $this->Membership_model->get_user_status();
        $data = array();
        $count = null;

        foreach($fetch_data as $row)
        {
           // $status_check = $this->category_model->get_status($row->status_id);
//            $select = '<select  name="status" class="form-control user_status">';
//            foreach ($status as $sts) {
//                $select.='<option  value="'.$sts->status_id.'">'.$sts->name.'</option>';
//            }
//            $select.='</select>';
            //echo form_open('user/delete');
            $count++;
            $sub_array = array();
            $sub_array[] = $count;
            $sub_array[] = $row->category_name;
            $sub_array[] = $row->created_by;
//                $sub_array[] =$select;
            $sub_array[] = '<button type="button" name="update_category" id="' . $row->category_id . '" class="btn btn-info btn-xs update_category"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
            $sub_array[] = '<button type="button"  name="delete"  value="' . $row->category_id . '" class="btn btn-danger btn-xs delete_category"/><i class="fa fa-times" aria-hidden="true"></i></button>';

            $data[] = $sub_array;
            //echo  form_close();
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->CategoryModel->get_all_data(),
            "recordsFiltered"     =>     $this->CategoryModel->get_filtered_data(),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }
    function fetch_single_user(){
        $output =array();
        $result =  $this->CategoryModel->fetch_single_user($_POST['category_id']);
        foreach ($result as $row){
            $output['category_name'] = $row->category_name;
            $output['created_by'] = $row->created_by;
            $output['category_id'] = $row->category_id;

        }
        echo json_encode($output);
    }


}
?>