<?php
require_once 'BaseController.php';
class ViewPostController extends BaseController {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('DonationPostModel');
    }
    function index(){

    }
    public function showpost(){
        $data = array();
        if($query = $this->DonationPostModel->showpost()){
//
            $data['records'] = $query;

            $data['main_content'] ='pages/showposts';
        }



      $this->load->view('includes/template',$data);


    }


}