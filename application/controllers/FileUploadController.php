<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class FileUploadController extends BaseController {
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('file_upload_model');
    }

    function index(){
        $data['main_content'] = 'pages/upload_form';
        $this->load->view('includes/template',$data);
    }
    function upload_document(){
        if($this->input->post('upload')){
            $file_path = $this->FileUploadModel->do_upload();

            $file_path_url = 'Uploaded_files/' . $file_path['file_name'];

            $image_data = array(
              'name' => $file_path_url,
                'type' =>$file_path['file_type'],
                'donation_id' => $_SESSION['donation_id']

            );
            $table = 'documents';
            $this->FileUploadModel->insert_document_data($table,$image_data);
            redirect('post');

        }
    }

    function viewpost(){
        $this->load->model('DonationPost_Model');
        $data['records']=$this->DonationPostModel->showpost();
        $data['main_content'] = 'pages/showposts';
        $this->load->view('includes/template',$data);

    }


}