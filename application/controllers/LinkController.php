<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'BaseController.php';
class LinkController extends BaseController {
    function __construct()
    {
        parent::__construct();
        $this->load->model('LinkModel');
    }
    function delete_link(){
        $id = $this->input->post('id');
        $this->LinkModel->delete_link($id);
    }


}