<?php
//if (!defined('BASEPATH'))
//    exit('No direct script access allowed');
class BaseController extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->is_log_in();
    }

    function is_log_in()
    {
        $is_log_in = $this->session->userdata('is_log_in');
        if (!isset($is_log_in) || $is_log_in != true) {
            echo "You don't have permisson. <a href='../login'>Login</a>";
            die();
        }

    }

    function logout(){
        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();
        redirect('login');

    }
}