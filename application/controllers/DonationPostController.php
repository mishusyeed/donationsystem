<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'BaseController.php';
class DonationPostController extends BaseController {
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('DonationPostModel');
        $this->load->model('FileUploadModel');
        $this->load->model('CategoryModel');
        $this->load->model('DonationTypeModel');
    }

    public function trace($trace){
        echo '<pre>';
        var_dump($trace);
        echo '</pre>';
    }
    public function index(){
        $data['category'] = $this->category_model->get_records();
        $data['donation_type'] = $this->Donation_type_Model->get_donation_type();
        $data['main_content']='pages/create_post';
        $data['nav'] = 'admin_navigation';
        $data['sidebar'] = 'super_admin_sidebar';
        $this->load->view('includes/template',$data);

    }

    function createDonation(){
        $uploadData = array();
        if($this->input->post('submit')){
            $data = array(
                'title' =>$this->input->post('title'),
                'description' => $this->input->post('description'),
                'donation_amount' =>$this->input->post('donation-amount'),
                'category_id' => $this->input->post('category_id'),
                'type_id' => $this->input->post('type_id'),
                'address' => $this->input->post('address'),
                'status_id'=>1,
                'created_by' => $_SESSION['username'],
                'created_at' =>date('Y-m-d H:i:s')
            );
            $table = 'donations';
            $inset_id_donation= $this->DonationPostModel->createDonation($table,$data);
            $donation=array(
                'donation_id' =>$inset_id_donation
            );
           $this->session->set_userdata($donation);



        }


        if($this->input->post('submit') && !empty($_FILES['photo']['name'])){
            var_dump($_FILES);


            $filesCount = count($_FILES['photo']['name']);
            for($i = 0; $i < $filesCount - 1; $i++){
                $a[0] = 7;
                $a = $a[0];
                $_FILES['p']['name'] = $_FILES['photo']['name'][$i];
                $_FILES['p']['type'] = $_FILES['photo']['type'][$i];
                $_FILES['p']['tmp_name'] = $_FILES['photo']['tmp_name'][$i];
                $_FILES['p']['error'] = $_FILES['photo']['error'][$i];
                $_FILES['p']['size'] = $_FILES['photo']['size'][$i];

                $uploadPath ='Uploaded_files/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('p')){
                    $fileData = $this->upload->data();
                    $uploadData['name'] ='Uploaded_files/'. $fileData['file_name'];
                    $uploadData['type'] = $fileData['file_type'];
                    $uploadData['donation_id'] = $_SESSION['donation_id'];
                    $uploadData['created_at'] = date("Y-m-d H:i:s");
                }


                if(!empty($uploadData)){
                    //Insert file information into the database
                    $insert = $this->FileUploadModel->insert($uploadData);
                    $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
                    $this->session->set_flashdata('statusMsg',$statusMsg);
                }


            }
            
        }
        if(isset($_POST['submit'])){
            if($_POST['link_field']){
                $link_value = $_POST['link_field'];
                foreach($link_value as $link){
                    $data =array(
                        'donation_id' => $_SESSION['donation_id'],
                        'link_detail'=>$link

                    );
                    $this->DonationPostModel->insert_link($data);


                }
            }
        }


        redirect('donation/view');
    }


    function post_table(){
       //$data ['records']= $this->DonationPost_Model->show_post_table();
        $data['main_content'] = 'pages/post_table';
        $data['nav'] = 'admin_navigation';
        $data['sidebar']= 'super_admin_sidebar';
        $this->load->view('includes/template',$data);
    }


    function viewpost(){
        $post_id = $this->uri->segment(2);
        $this->load->model('DonationPostModel');
        $donation = $this->DonationPostModel->showpost($post_id);
        $result =$donation->result_array();
        $id=null;
        if(count($result)>0) {
            $id = $result[0]['donation_id'];
        }
       if(isset($id) && $id !=null){
           $data['documents'] = $this->DonationPostModel->show_document($id);
           $data['records']=$this->DonationPostModel->showpost($post_id);
           $data['status'] = $this->DonationPostModel->get_status();
           $data['main_content'] = 'pages/showposts';
           $data['nav'] = 'admin_navigation';
           $data['sidebar']= 'super_admin_sidebar';
           $this->load->view('includes/template',$data);
       }else{
           $data['records']=$this->DonationPostModel->showpost();
           $data['main_content'] = 'pages/showposts';
           $data['nav'] = 'admin_navigation';
           $data['sidebar']= 'super_admin_sidebar';
           $this->load->view('includes/template',$data);

       }


    }


    function update_post_data(){
        $id = $this->uri->segment(3);
        $data['nav'] ='admin_navigation';
        $data['category'] = $this->CategoryModel->get_records();
        $data['sidebar'] = 'super_admin_sidebar';
        $data['main_content'] ='pages/update-post';
        $data['records'] = $this->DonationPostModel->get_data_for_update($id);
        $data['documents'] = $this->DonationPostModel->show_document($id);
        $data['donation_type'] = $this->Donation_type_Model->get_donation_type();
        $data['link']=$this->DonationPostModel->get_all_link_by_id($id);
        $this->load->view('includes/template',$data);

    }


    function edit_post(){

            if($this->input->post('submit')){
                $data = array(
                    'title' =>$this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'donation_amount' =>$this->input->post('donation-amount'),
                    'category_id' => $this->input->post('category_id'),
                    'type_id' => $this->input->post('type_id'),
                    'address' => $this->input->post('address'),
                );
                $id = $this->input->post('hidden');
                $this->DonationPostModel->update_post($id,$data);


            }

        if($this->input->post('submit')){

            $filesCount = count($_FILES['photo']['name']);
            for($i = 0; $i < $filesCount - 1; $i++){

                $_FILES['p']['name'] = $_FILES['photo']['name'][$i];
                $_FILES['p']['type'] = $_FILES['photo']['type'][$i];
                $_FILES['p']['tmp_name'] = $_FILES['photo']['tmp_name'][$i];
                $_FILES['p']['error'] = $_FILES['photo']['error'][$i];
                $_FILES['p']['size'] = $_FILES['photo']['size'][$i];

                $uploadPath ='Uploaded_files/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('p')){
                    $fileData = $this->upload->data();
                    $uploadData['name'] ='Uploaded_files/'. $fileData['file_name'];
                    $uploadData['type'] = $fileData['file_type'];
                    $uploadData['donation_id'] = $this->input->post('hidden');
                    $uploadData['created_at'] = date("Y-m-d H:i:s");
                }


                if(!empty($uploadData)){
                    //Insert file information into the database
                    $insert = $this->FileUploadModel->insert($uploadData);
                    $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
                    $this->session->set_flashdata('statusMsg',$statusMsg);
                }


            }

        }


        if ($this->input->post('submit')) {
            $donation_id = $this->input->post('hidden');
            $link_value = $_POST['link_field'];
            $link_id = $_POST['link_id'];
            $i = 0;
            $size = count($link_id);
            while ($i < $size) {
                $get_link_id = $this->DonationPostModel->get_link($link_id[$i]);
                if($link_id[$i] !='') {
                    if ($get_link_id['link_id'] == $link_id[$i]) {
                        if ($link_value[$i] != '') {
                            $data = array(
                                'link_detail' => $link_value[$i]
                            );
                            $this->DonationPostModel->update_link($link_id[$i], $data);
                        }

                    }
                }else {
                        if(!empty($link_value[$i])){
                            $data=array(
                                'link_detail'=>$link_value[$i],
                                'donation_id'=>$donation_id
                            );
                            $this->DonationPostModel->insert_link($data);
                        }

                }

                ++$i;
            }



        }
            redirect('donation/view');

    }
    function post_delete(){
        if($this->input->post('delete')){
            $id = $this->input->post('id');
            $this->DonationPostModel->delete($id);
            if($this->DonationPostModel->delete($id)){
                redirect('donation/view');
            }
           //
        }
    }

        function status_update(){
        $id = $this->input->post('donation_id');
            $data = array(
                'status_id' =>$this->input->post('check')
            );
            $this->DonationPostModel->update_status($id,$data);
            echo "Updated";
        }


    function donation_data_table(){
        $fetch_data = $this->DonationPostModel->make_datatables();
        $data = array();
        $count = null;

        foreach($fetch_data as $row)
        {
           ;
            $count++;
            $sub_array = array();
            $sub_array[] = $count;
            $sub_array[] = $row->title;
            $sub_array[] = $row->description;
            $sub_array[] = $row->donation_amount;
            $sub_array[] = $row->created_at;

            $sub_array[] = '<button type="button" value="'. $row->donation_id .'" name="view" id="view" class="btn btn-info btn-xs view">View</button>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->DonationPostModel->get_all_data(),
            "recordsFiltered"     =>     $this->DonationPostModel->get_filtered_data(),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }





}