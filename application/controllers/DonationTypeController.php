<?php
require_once 'BaseController.php';
class DonationTypeController extends BaseController {
    function __construct()
    {

        parent::__construct();
        $this->load->library('session');
        $this->load->model('DonationTypeModel');
    }

    function donation_type_list(){
        $data['main_content'] = 'pages/type_list';
        $data['nav'] = 'admin_navigation';
        $data['sidebar']= 'super_admin_sidebar';
        $this->load->view('includes/template',$data);
    }
    function donation_type_data_table(){
        $fetch_data = $this->DonationTypeModel->make_datatables();
        $data = array();
        $count = null;

        foreach($fetch_data as $row)
        {
            ;
            $count++;
            $sub_array = array();
            $sub_array[] = $count;
            $sub_array[] = $row->type_name;


            $sub_array[] = '<button type="button" id="'. $row->type_id .'" name="update_type"  class="btn btn-info btn-xs update_type_val"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</button>';
            $sub_array[] = '<button type="button" value="" name="delete_type" id="'. $row->type_id .'" class="btn btn-danger btn-xs delete_type"><i class="fa fa-times" aria-hidden="true"></i>
</button>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->DonationTypeModel->get_all_data(),
            "recordsFiltered"     =>     $this->DonationTypeModel->get_filtered_data(),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }
    function fetch_single_type(){
        $id = $this->input->post('type_id');
        $data = $this->DonationTypeModel->fetch_single_type($id);
        echo json_encode($data);
    }
    function add_type(){
        if($this->input->post('btn')=='Add'){
            $data =array(
                'type_name'=>$this->input->post('type_name')
            );
            $this->DonationTypeModel->inset_type($data);
            echo 'successfully added';
        }

    }
    function update_type(){
if($this->input->post('submit')=='Edit'){
    $id = $this->input->post('hidden');
    $data = array(
        'type_name'=>$this->input->post('type_name')
    );
    $this->DonationTypeModel->update_type($id,$data);
    echo "successfully upadted";
}

    }
    function delete_type(){
        $id = $this->input->post('id');
        $this->DonationTypeModel->delete_type($id);
    }



}