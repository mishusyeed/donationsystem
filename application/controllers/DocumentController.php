<?php
class DocumentController extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('DocumentModel');
    }
    function delete_document(){
        $id = $this->input->post('id');
        $this->DocumentModel->delete_document($id);
        echo "Doc delete";
    }

}