<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once 'BaseController.php';
class RoleController extends BaseController {
    public function __construct()
    {
        parent::__construct();
        //$this->load->helper('url');
        //$this->load->model('role_model');

    }
    public function index(){
        $this->load->view('includes/header');
        $this->load->view('pages/CreateRole');
        $this->load->view('includes/footer');
    }
    public function add_role(){
        $this->load->model('RoleModel');
        $data = array(
            'name' => $this->input->post('name'),
            'created_by' => $this->input->post('created_by'),
            'created_at' => date('Y-m-d H:i:s')
        );
        $table = 'roles';
        $this->RoleModel->add_role($table,$data);
        return $this->index();

    }
}
?>