<?php
class DonationPostUser extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('DonationPostModel');
    }
    function donation_post_user_view(){
        $documents = array();
        $data['main_content'] = 'pages/show_post_user';

        $records = $this->DonationPostModel->donation_post_user_view();

        foreach ($records as $record){
            $documents= $this->DonationPostModel->show_document($record->donation_id);
            $record->documents= $documents;
//            $record["documents"]= $documents;
        }
        $data['records']=$records;
        $query = $this->DonationPostModel->donation_post_user_view();

        $this->load->view('includes/template',$data);
    }
}